import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import counter from './counter';
import foodItems from './foodItems';

export default combineReducers({
  router: routerReducer,
  counter,
  foodItems
});
