export const UPDATE_FOOD_ITEMS = 'foodItems/UPDATE_FOOD_ITEMS';
export const ADD_FOOD_ITEM = 'foodItems/ADD_FOOD_ITEM';
export const REMOVE_FOOD_ITEM = 'foodItems/REMOVE_FOOD_ITEM';

const initialState = {
  initialItems: [],
  shoppingCart: {}
};

export default (state = initialState, action) => {
  let shoppingCart;

  switch (action.type) {
    case UPDATE_FOOD_ITEMS:
      shoppingCart = {};
      action.newList.forEach(item => (shoppingCart[item.name] = 0));
      return {
        ...state,
        initialItems: action.newList,
        shoppingCart: shoppingCart
      };

    case ADD_FOOD_ITEM:
      shoppingCart = state.shoppingCart;
      shoppingCart[action.itemName] += 1;
      return {
        ...state,
        shoppingCart: shoppingCart
      };

    case REMOVE_FOOD_ITEM:
      shoppingCart = state.shoppingCart;
      shoppingCart[action.itemName] -= 1;
      return {
        ...state,
        shoppingCart: shoppingCart
      };

    default:
      return state;
  }
};

export const updateFoodItems = newList => {
  return dispatch => {
    dispatch({
      type: UPDATE_FOOD_ITEMS,
      newList: newList
    });
  };
};

export const addFoodItems = itemName => {
  return dispatch => {
    dispatch({
      type: ADD_FOOD_ITEM,
      itemName: itemName
    });
  };
};

export const removeFoodItems = itemName => {
  return dispatch => {
    dispatch({
      type: REMOVE_FOOD_ITEM,
      itemName: itemName
    });
  };
};
