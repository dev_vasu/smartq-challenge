import React from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Forward from '@material-ui/icons/Forward';

class LeftPanel extends React.Component {
  render() {
    return (
      <div>
        <List component="nav">
          {this.props.foodList.map((item, index) => {
            return (
              <ListItem key={index} button>
                <ListItemIcon>
                  <Forward />
                </ListItemIcon>
                <ListItemText primary={item} />
              </ListItem>
            );
          })}
        </List>
      </div>
    );
  }
}

const filterCategories = items => {
  let filteredItems = [];

  items.forEach(item => {
    if (!filteredItems.includes(item.category)) {
      filteredItems.push(item.category);
    }
  });

  return filteredItems;
};

const mapStateToProps = state => ({
  foodList: filterCategories(state.foodItems.initialItems)
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftPanel);
