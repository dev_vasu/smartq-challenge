import React from 'react';
import Forward from '@material-ui/icons/FiberManualRecord';
import { withStyles } from '@material-ui/core/styles';

const styleForRedIcon = {
  root: {
    color: 'red'
  }
};

function RedIcon(props) {
  const { classes } = props;

  return (
    <Forward
      classes={{
        root: classes.root, // class name, e.g. `classes-nesting-root-x`
        label: classes.label // class name, e.g. `classes-nesting-label-x`
      }}
    />
  );
}

export default withStyles(styleForRedIcon)(RedIcon);
