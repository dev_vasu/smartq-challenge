import React from 'react';
import GreenIcon from './green-icon';
import RedIcon from './red-icon';
import Add from '@material-ui/icons/Add';
import Remove from '@material-ui/icons/Remove';
import Card from '@material-ui/core/Card';

export default class SingleItem extends React.Component {
  render() {
    return (
      <Card className="single-item">
        {Boolean(this.props.item.vegflag === 'nonveg') && <RedIcon />}
        {Boolean(this.props.item.vegflag === 'veg') && <GreenIcon />}
        <div className="item-name-desc">
          <h3>{this.props.item.name}</h3>
          <p>{this.props.item.description || 'No Description'}</p>
        </div>
        <Add
          onClick={e => {
            this.props.add(this.props.item.name);
          }}
        />
        <p>{this.props.shoppingCart[this.props.item.name]}</p>
        <Remove
          onClick={e => {
            this.props.remove(this.props.item.name);
          }}
        />
        <p>{this.props.item.price}</p>
      </Card>
    );
  }
}
