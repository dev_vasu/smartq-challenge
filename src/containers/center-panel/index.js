import React from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import SingleItem from './single-item';
import { bindActionCreators } from 'redux';
import { addFoodItems, removeFoodItems } from '../../modules/foodItems';

const SingleCategory = props => {
  return (
    <Card>
      <Typography variant="headline" color="textSecondary">
        {props.category}
      </Typography>
      <List component="ul">
        {props.foodItems.map((item, index) => {
          return (
            Boolean(props.category === item.category) && (
              <ListItem key={index}>
                <SingleItem
                  item={item}
                  shoppingCart={props.shoppingCart}
                  add={props.addFoodItems}
                  remove={props.removeFoodItems}
                />
              </ListItem>
            )
          );
        })}
      </List>
    </Card>
  );
};

class CenterPanel extends React.Component {
  render() {
    return (
      <div>
        <List component="nav">
          {this.props.categories.map((item, index) => {
            return (
              <SingleCategory
                key={index}
                category={item}
                foodItems={this.props.foodItems}
                shoppingCart={this.props.shoppingCart}
                addFoodItems={this.props.addFoodItems}
                removeFoodItems={this.props.removeFoodItems}
              />
            );
          })}
        </List>
      </div>
    );
  }
}

const filterCategories = items => {
  let filteredItems = [];

  items.forEach(item => {
    if (!filteredItems.includes(item.category)) {
      filteredItems.push(item.category);
    }
  });

  return filteredItems;
};

const mapStateToProps = state => ({
  categories: filterCategories(state.foodItems.initialItems),
  foodItems: state.foodItems.initialItems,
  shoppingCart: state.foodItems.shoppingCart
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addFoodItems,
      removeFoodItems
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CenterPanel);
