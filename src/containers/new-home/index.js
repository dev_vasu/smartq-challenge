import React from 'react';
import LeftPanel from '../left-panel';
import CenterPanel from '../center-panel';
import RightPanel from '../right-panel';

const NewHome = () => (
  <div className="container">
    <LeftPanel />
    <CenterPanel />
    <RightPanel />
  </div>
);

export default NewHome;
